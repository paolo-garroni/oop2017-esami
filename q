[33mcommit d6521643f6c77498ab2d5bf75e08c4cfdf1c285d[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Author: Paolo Garroni <garroni.paolo@gmail.com>
Date:   Sun Dec 30 17:00:31 2018 +0100

    Completes a02a

[33mcommit 555335f37c82a3fcc5e7bd31ee312062857a6e49[m
Author: mirko <mirko.viroli@unibo.it>
Date:   Sun Dec 16 22:33:09 2018 +0100

    appello 06

[33mcommit 08dd15badd1d4f76c6079a620c3b7b7b52961df7[m
Author: Mirko Viroli <mirko.viroli@unibo.it>
Date:   Wed Jul 4 12:12:09 2018 +0200

    appello 05

[33mcommit 98b4bf12afe4da141c77ffcd3e9dd1af63793efe[m
Author: Mirko Viroli <mirko.viroli@unibo.it>
Date:   Wed Jun 20 15:07:25 2018 +0200

    appello 04

[33mcommit 8bd0e15fcb0b718b15c83df44f33a873b7f47564[m
Author: Mirko Viroli <mirko.viroli@unibo.it>
Date:   Fri Feb 16 18:34:48 2018 +0100

    appello 03

[33mcommit 5ba0a7481767fcd6fdad8bb033a45f14a28aaa81[m
Author: Mirko Viroli <mirko.viroli@unibo.it>
Date:   Tue Jan 30 16:25:53 2018 +0100

    refactoring packages

[33mcommit ac98c9176282b7134760fc6418412029fc5cfff7[m
Author: Mirko Viroli <mirko.viroli@unibo.it>
Date:   Fri Jan 26 23:30:17 2018 +0100

    refactored a01, added a02

[33mcommit 5575d73dbfd4efb961380e6004b2f300b522b541[m
Author: Mirko Viroli <mirko.viroli@unibo.it>
Date:   Thu Jan 11 22:11:46 2018 +0100

    appello 01

[33mcommit 1540bd373d38b874333add6b8e55bc4ab5f76904[m
Author: Mirko Viroli <mirko.viroli@unibo.it>
Date:   Thu Jan 11 22:11:20 2018 +0100

    added empty gitignore
