package a01b.e1;

import java.util.function.Supplier;

public class TraceFactoryImpl implements TraceFactory {

	public TraceFactoryImpl() {}

	@Override
	public <X> Trace<X> fromSuppliers(Supplier<Integer> sdeltaTime, Supplier<X> svalue, int size) {
		return new TraceImpl<X>(sdeltaTime, svalue, size);
	}

	@Override
	public <X> Trace<X> constant(Supplier<Integer> sdeltaTime, X value, int size) {
		return new TraceImpl<X>(sdeltaTime, () -> value, size);
	}

	@Override
	public <X> Trace<X> discrete(Supplier<X> svalue, int size) {
		return new TraceImpl<X>(() -> 1, svalue, size);
	}

}
