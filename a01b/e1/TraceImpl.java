package a01b.e1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

public class TraceImpl<X> implements Trace<X> {
	
	Iterator<Event<X>> iterator;
	
	public TraceImpl(Supplier<Integer> sdeltaTime, Supplier<X> svalue, int size) {
		this.iterator = new Iterator<Event<X>>() {
			int index = 0;

			@Override
			public boolean hasNext() {
				return index < size ? true : false;
			}

			@Override
			public Event<X> next() {
				EventImpl<X> event = new EventImpl<X>(sdeltaTime.get() * index, svalue.get());
				index += 1;
				return event;
			}
		};
	}
	
	public TraceImpl(Iterator<Event<X>> iterator) {
		this.iterator = iterator;
	}

	@Override
	public Optional<Event<X>> nextEvent() {
		return this.iterator.hasNext() ? Optional.of(this.iterator.next()) : Optional.empty();
	}

	@Override
	public Iterator<Event<X>> iterator() {
		return this.iterator;
	}

	@Override
	public void skipAfter(int time) {
		while (this.iterator.hasNext() && this.iterator.next().getTime() < time) {;}
	}

	@Override
	public Trace<X> combineWith(Trace<X> trace) {
		List<Event<X>> list = new ArrayList<>();
		this.iterator.forEachRemaining(list::add);
		trace.iterator().forEachRemaining(list::add);
		list.sort(Comparator.comparing(Event::getTime));
		return new TraceImpl<>(list.iterator());
	}

	@Override
	public Trace<X> dropValues(X value) {
		List<Event<X>> list = new ArrayList<>();
		this.iterator.forEachRemaining(event -> {
			if (event.getValue() != value) {
				list.add(event);
			}
		});
		return new TraceImpl<>(list.iterator());
	}

}
