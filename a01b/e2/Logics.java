package a01b.e2;

import java.util.List;

public interface Logics {
	
	/**
	 * 
	 * @return the size of the values list
	 */
	int getSize();
	
	/**
	 * 
	 * @return the list of all values
	 */
	List<Integer> getValues();
	
	/**
	 * 
	 * @param the index of the element to test
	 * @return true if the element at the given index is the minimum in the values list
	 */
	boolean attempt(int index);
	
	/**
	 * 
	 * @return the number of attempts
	 */
	int getAttemptsNumber();
}
