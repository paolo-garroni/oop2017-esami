package a01b.e2;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.*;
import javax.swing.*;

public class GUI extends JFrame{

	private static final long serialVersionUID = 1L;
	
	List<JButton> buttons;
	Logics logics;
	
	public GUI(int size){
		
		/* Creates the logic object */
		this.logics = new LogicsImpl(size);
		
		/* Sets main frame look */
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		
		/* Creates the buttons and sets their behavior */
		this.buttons = new ArrayList<>(); 
		IntStream.range(0, size).forEach(index -> {
			JButton b = new JButton("*");
			this.buttons.add(b);
			this.getContentPane().add(b);
			b.addActionListener(e -> {
				if (this.logics.attempt(index)) {
					b.setText(String.valueOf(this.logics.getValues().get(index)));
					b.setEnabled(false);
				}
				System.out.println("Attempts: " + this.logics.getAttemptsNumber());
			});

		});
		
		/* Renders the GUI */
		this.setVisible(true);
	}
	
}
