package a01b.e2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class LogicsImpl implements Logics {
	
	static class Value {
		
		enum State {
			ENABLED, DISABLED;
		}		
		
		private int value;
		private State state;
		
		private Value(int value) {
			this.value = value;
			this.state = State.ENABLED;
		}
		
		boolean isEnabled() {
			return this.state == State.ENABLED;
		}
		
		void disable() {
			this.state = State.DISABLED;
		}
		
		int getValue() {
			return this.value;
		}
				
	}
	
	List<Value> values;
	int attempts;

	public LogicsImpl(int size) {
		this.attempts = 0;
		this.values = new ArrayList<>(size);
		IntStream.range(0, size).forEach(n -> {
			this.values.add(new Value(
					(int) Math.round(Math.random() * 100)));
		});
	}

	@Override
	public int getSize() {
		return this.values.size();
	}

	@Override
	public List<Integer> getValues() {
		return this.values.stream()
						  .map(value -> value.getValue())
						  .collect(Collectors.toList());
	}

	@Override
	public boolean attempt(int index) {
		this.attempts += 1;
		if (this.values.get(index).isEnabled() 
				&& this.values.get(index).getValue() 
					== this.values.stream()
								  .filter(Value::isEnabled)
								  .mapToInt(Value::getValue)
								  .min()
								  .getAsInt()) {
			this.values.get(index).disable();
			return true;
		};
		return false;
	}

	@Override
	public int getAttemptsNumber() {
		return this.attempts;
	}

}
