package a02b.e2;

public interface Logics {

	/**
	 * Deletes a String from the file 
	 * @param line the selected line
	 */
	String remove(String line);
	
	/**
	 * Duplicates the selected line
	 * @param line the selected line
	 */
	String concat(String line);
	
	/**
	 * Adds a new line which is the selected line with "*" as a prefix
	 * @param line the selected line
	 */
	String add(String line);

	/**
	 * Reads all lines from file 
	 */
	String getFileContent();
	
	/**
	 * Creates file
	 */
	void createFile();
	
}
