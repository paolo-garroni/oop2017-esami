package a02b.e2;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class LogicsImpl implements Logics {
	
	private final String fileName;

	public LogicsImpl(String filename) {
		this.fileName = Objects.requireNonNull(filename);
	}
	
	@Override
	public String getFileContent() {
		String fileContent = "";
		try {
			fileContent = Files.lines(Paths.get(fileName, "")).collect(Collectors.joining("\n"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fileContent;
	}

	@Override
	public String remove(String line) {
		try {
			String newContent = Arrays.asList(getFileContent().split("\n")).stream()
					.filter(s -> !s.contains(line))
					.collect(Collectors.joining("\n"));
			PrintStream ps = new PrintStream(fileName);
			ps.print(newContent);
			ps.close();
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
		return new String();
	}

	@Override
	public String concat(String line) {
		try {
			String newContent = getFileContent().replace(line, line + line);
			PrintStream ps = new PrintStream(fileName);
			ps.print(newContent);
			ps.close();
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
		return line + line;
	}

	@Override
	public String add(String line) {
		try {
			String newContent = getFileContent().concat("\n*" + line);
			PrintStream ps = new PrintStream(fileName);
			ps.print(newContent);
			ps.close();
			return "*" + line;
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}

	@Override
	public void createFile() {
		try {
			PrintStream ps = new PrintStream(fileName);
			ps.print("line1\nline2\nline3\nline4\n");
			ps.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
