package a02b.e2;

import java.io.*;
import java.util.List;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.Function;
import java.awt.*;
import javax.swing.*;

public class GUI {
	
	Logics logics;
	final JButton remove = new JButton("Remove");
	final JButton concat = new JButton("Concat");
	final JButton add = new JButton("Add");
	final JFrame gui = new JFrame();
	final JComboBox<String> combo = new JComboBox<>();

	public GUI(String fileName) throws IOException {
		
		/* Create logics object */
		this.logics = new LogicsImpl(Objects.requireNonNull(fileName));
		this.logics.createFile();
		
		/* Create components and main frame */
		comboReset(combo);
		final JPanel main = new JPanel();
		main.setLayout(new FlowLayout());
		main.add(combo);
		main.add(remove);
		main.add(concat);
		main.add(add);
		gui.getContentPane().add(main);
		gui.pack();

		/* Create behaviour */
		gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addButtonListener(remove, logics::remove);
		addButtonListener(concat, logics::concat);
		addButtonListener(add, logics::add);
		
		/* Set visible */ 
		gui.setVisible(true);
	}

	private void comboReset(JComboBox<String> combo) {
		SwingUtilities.invokeLater(() -> {
			List<String> lines = Arrays.asList(logics.getFileContent().split("\n"));
			combo.removeAllItems();
			lines.forEach(combo::addItem);
			combo.setSelectedIndex(0);			
		});
	}
	
	private void addButtonListener(JButton button, Function<String, String> strategy) {
		button.addActionListener(e -> {
			SwingUtilities.invokeLater(() -> {
				String result = strategy.apply(((String) combo.getSelectedItem()));
				System.out.println(result);
				comboReset(combo);
				gui.getContentPane().repaint();
			});
		});
	}
}
