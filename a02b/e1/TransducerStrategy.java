package a02b.e1;

import java.util.List;

public interface TransducerStrategy<X,Y> {
	
	Y build(List<X> xList);
	
}
