package a02b.e1;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TransducerFactoryImpl implements TransducerFactory {
	
	public TransducerFactoryImpl() {}

	@Override
	public <X> Transducer<X, String> makeConcatenator(int inputSize) {
		return new TransducerImpl<>(
					xList -> xList.stream()
								  .map(String::valueOf)
								  .collect(Collectors.joining()), 
					inputSize);
	}

	@Override
	public Transducer<Integer, Integer> makePairSummer() {
		return new TransducerImpl<>(
				xList -> xList.stream().collect(Collectors.summingInt(n -> n)),
				2);
	}

}
