package a02b.e1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class TransducerImpl<X, Y> implements Transducer<X, Y> {
	
	private final List<X> in;
	private boolean isInputOver;
	private final List<Y> out;
	private final TransducerStrategy<X, Y> strategy;
	private final int blockSize;

	public TransducerImpl(TransducerStrategy<X, Y> strategy, int blockSize) {
		this.in = new LinkedList<X>();
		this.out = new LinkedList<Y>();
		this.strategy = strategy;
		this.blockSize = blockSize;
		this.isInputOver = false;
	}

	@Override
	public void provideNextInput(X x) {
		in.add(Objects.requireNonNull(x));
		if (in.size() >= blockSize) {
			out.add(strategy.build(getInputBlock()));
		}
	}

	@Override
	public void inputIsOver() {
		if (this.isInputOver) {
			throw new IllegalStateException("Input already closed");
		}
		this.isInputOver = true;
		if (in.size() > 0) {
			out.add(strategy.build(getInputBlock()));
		}
	}

	@Override
	public boolean isNextOutputReady() {
		return (in.size() >= blockSize || !out.isEmpty()) ? true : false;
	}

	@Override
	public Y getOutputElement() {
		if (isNextOutputReady()) {
			return out.remove(0);
		}
		else if (!isInputOver && !out.isEmpty()) {
			return strategy.build(getInputBlock());
		}
		throw new IllegalStateException("Output is over");
	}

	@Override
	public boolean isOutputOver() {
		return !isNextOutputReady() && isInputOver;
	}

	private List<X> getInputBlock() {
		if (in.size() >= blockSize || isInputOver) {
			List<X> block = new ArrayList<X>();
			in.stream().limit(blockSize).forEach(block::add);
			in.removeAll(block);
			return block;
		}
		else throw new IllegalStateException("Input size is lower than the specified block size");
	}
}
