package a02a.e2;

public interface Logics {
	
	enum State { ENABLED, DISABLED };
	
	void hit(Pair<Integer, Integer> position);
	
	State getElementState(Pair<Integer, Integer> position);

	boolean toBeClosed();
	
}
