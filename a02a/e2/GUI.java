package a02a.e2;

import java.util.List;
import java.util.stream.IntStream;

import javax.swing.*;

import a02a.e2.Logics.State;

import java.util.*;
import java.awt.*;

public class GUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private final Logics logic;
	private final List<List<MyButton>> buttons;
	
	private static class MyButton extends JButton {
		
		private static final long serialVersionUID = 1L;
		private final Pair<Integer, Integer> position;
		
		private MyButton(final Integer x, final Integer y) {
			this.position = new Pair<>(x, y);
		}
		private Pair<Integer, Integer> getPosition() {
			return this.position;
		}
	}
    
    public GUI(int size) {
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        int cols = size; 
        JPanel panel = new JPanel(new GridLayout(cols,cols));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
    	logic = new LogicsImpl(size);
    	buttons = new ArrayList<>(size);
    	IntStream.range(0, size).forEach(n -> buttons.add(new ArrayList<>(size)));
    	IntStream.range(0, size).forEach(y -> {
    		List<MyButton> row = new ArrayList<>(size);
    		IntStream.range(0, size).forEach(n -> row.add(new MyButton(0, 0)));;
    		buttons.set(y, row);
    		IntStream.range(0, size).forEach(x -> {
    			MyButton bt = new MyButton(x, y);
    			row.set(x, bt);
        		bt.addActionListener(e -> {
                    logic.hit(bt.getPosition());
                    if (logic.toBeClosed()) {
                    	System.exit(0);
                    }
                    bt.setText(
                    		logic.getElementState(bt.getPosition()) == State.ENABLED ?
                    				"*" : " "
                    		);
                });
        		panel.add(bt);
    		});
    	});
    
        this.setVisible(true);
    }
    
}
