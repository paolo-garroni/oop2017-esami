package a02a.e2;

import java.util.*;
import java.util.stream.*;

public class LogicsImpl implements Logics {

	private final List<List<State>> matrix;

	public LogicsImpl(final int size) {
		this.matrix = new ArrayList<>();
		for (int y=0; y<size; y++) {
			List<State> row = new ArrayList<>();
			matrix.add(row);
			for(int x=0; x<size; x++) {
				row.add(State.DISABLED);
			}
		}
	}

	@Override
	public void hit(Pair<Integer, Integer> position) {
		this.matrix.get(position.getX()).set(position.getY(),
				this.getElementState(position) == State.ENABLED ? State.DISABLED : State.ENABLED);
	}

	@Override
	public State getElementState(Pair<Integer, Integer> position) {
		return this.matrix.get(position.getX()).get(position.getY());
	}

	@Override
	public boolean toBeClosed() {
		if (IntStream.range(0, this.matrix.size())
				.anyMatch(n -> this.matrix.get(n).stream().allMatch(s -> s == State.ENABLED)
						|| this.matrix.stream().map(l -> l.get(n)).allMatch(s -> s == State.ENABLED))) {
			return true;
		}
		return false;
	}

}
