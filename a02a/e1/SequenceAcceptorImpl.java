package a02a.e1;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SequenceAcceptorImpl implements SequenceAcceptor {
	
	private Optional<Iterator<Integer>> iterator;
	private Optional<Sequence> sequence;

	public SequenceAcceptorImpl() {
		this.iterator = Optional.empty();
		this.sequence = Optional.empty();
	}

	@Override
	public void reset(Sequence sequence) {
		this.sequence = Optional.of(sequence);
		if (sequence == Sequence.POWER2) {
			power2Init();
		} else if (sequence == Sequence.FLIP) {
			flipInit();
		} else if (sequence == Sequence.RAMBLE) {
			rambleInit();
		} else if (sequence == Sequence.FIBONACCI) {
			fibonacciInit();
		}
	}

	@Override
	public void reset() {
		if (this.sequence.isPresent()) {
			reset(this.sequence.get());
		}
		else {
			throw new IllegalStateException("Sequence not initialized");
		}
	}

	@Override
	public void acceptElement(int i) {
		if (iterator.isPresent()) {
			int next = iterator.get().next();
			if (next != i) {
				throw new IllegalStateException(
						"The element to be accepted in the sequence " + sequence.get() 
						+ " should have been " + next 
						+ " but was " + i);
			}
		} else {
			throw new IllegalStateException("Iterator not initialized");
		}
	}
	
	private void power2Init() {
		this.iterator = Optional.of(IntStream.iterate(0, n -> n + 1)
				 .mapToObj(n -> Integer.valueOf((int) Math.pow(2, n)))
				 .iterator());
	}
	
	private void flipInit() {
		this.iterator = Optional.of(IntStream.iterate(1, n -> n == 1 ? 0 : 1).iterator());
	}
	
	private void rambleInit() {
		this.iterator = Optional.of(IntStream.iterate(1, n -> n + 1)
											 .mapToObj(n -> new Pair<Integer,Integer>(0, n))
											 .map(pair -> Arrays.asList(pair.getX(), pair.getY()))
											 .flatMap(List::stream)
											 .iterator());
	}
	
	private void fibonacciInit() {
		this.iterator = Optional.of(Stream
				.iterate(new Pair<Integer, Integer>(1,1), pair -> new Pair<>(
						pair.getX() + pair.getY(),
						pair.getX() + pair.getY() + pair.getY()))
				.flatMap(pair -> Arrays.asList(pair.getX(), pair.getY()).stream())
				.iterator());
	}

}
