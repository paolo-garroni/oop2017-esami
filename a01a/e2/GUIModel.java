package a01a.e2;

import java.util.List;

public interface GUIModel {
	
	int getSize();
	
	void hit(int index);
	
	List<Integer> getValues();
		
	boolean toBeClosed();
	
}
