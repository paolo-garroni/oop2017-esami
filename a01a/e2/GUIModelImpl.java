package a01a.e2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

public class GUIModelImpl implements GUIModel {
	
	private final List<Integer> values;

	/**
	 * @param The number of values to be stored in the list.
	 */
	public GUIModelImpl(final int size) {
		this.values = new ArrayList<Integer>(size);
		IntStream.range(0, size).forEach(n -> this.values.add(new Integer(0)));
	}

	/**
	 * Returns the size of the list.
	 * @return The number of values stored in the list.
	 */
	@Override
	public int getSize() {
		return this.values.size();
	}

	/**
	 * Increments the value at the given index.
	 * @param index The index of the element hit.
	 */
	@Override
	public void hit(int index) {
		this.values.set(index, this.values.get(index) + 1);
	}

	/**
	 * Returns all values.
	 * @return An ordered list containing all stored values.
	 */
	@Override
	public List<Integer> getValues() {
		return Collections.unmodifiableList(this.values);
	}

	/**
	 * Test to determine if the program is to be closed.
	 * @return true if the program is to be closed.
	 */
	@Override
	public boolean toBeClosed() {
		return this.values.stream().sequential().allMatch(el1 -> el1.equals(this.values.get(0)));
	}

}
