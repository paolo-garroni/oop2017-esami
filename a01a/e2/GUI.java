package a01a.e2;

import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.swing.*;

public class GUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private final List<JButton> buttons;
	private final JButton printButton;
	private final GUIModel model;

	public GUI(GUIModel model) {
		this.model = model;
		this.buttons = new ArrayList<JButton>();
		this.printButton = new JButton("Print");
		this.setMainFrameLook();
		this.createButtons();		
		this.setBehaviour();
		this.setVisible(true);
	}

	
	/**
	 * Sets the main frame look
	 */
	private void setMainFrameLook() {
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
	}
	
	/**
	 * Creates the buttons
	 */
	private void createButtons() {
		IntStream.range(0, this.model.getSize())
				 .mapToObj(n -> new JButton("0"))
				 .forEach(b -> {
					this.buttons.add(b); 
					this.getContentPane().add(b);
				 });
		this.getContentPane().add(this.printButton);
	}
	
	/**
	 * Sets the behaviour
	 */
	private void setBehaviour() {
		this.buttons.forEach(btn -> {
			btn.addActionListener(e -> {
				JButton button = (JButton)e.getSource();
				int index = GUI.this.buttons.indexOf(button);
				GUI.this.model.hit(index);
				IntStream.range(0, GUI.this.model.getSize())
						 .forEach(n -> {
							 GUI.this.buttons.get(n).setText(String.valueOf( 
									 GUI.this.model.getValues().get(n)));
						 });
				if (GUI.this.model.toBeClosed()) {
					System.exit(0);
				}
			});
		});
		printButton.addActionListener(e -> {
			System.out.println(this);
		});
	}
	
	
	@Override
	public String toString() {
		return buttons.stream()
					  .map(b -> b.getText())
					  .collect(Collectors.joining("|", "<", ">"));
	}

}
