package a01a.e1;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class InfiniteSequenceOpsImpl implements InfiniteSequenceOps {

	public InfiniteSequenceOpsImpl() {
	}

	@Override
	public <X> InfiniteSequence<X> ofValue(X x) {
		return () -> x;
	}

	@Override
	public <X> InfiniteSequence<X> ofValues(List<X> l) {
				
		return new InfiniteSequence<X>() {
			Iterator<X> iterator = Objects.requireNonNull(l).iterator();

			@Override
			public X nextElement() {
				if (!iterator.hasNext()) {
					iterator = l.iterator();
				}
				return iterator.next();
			}
		};
	}

	@Override
	public InfiniteSequence<Double> averageOnInterval(InfiniteSequence<Double> iseq, int intervalSize) {
			return () -> iseq.nextListOfElements(intervalSize)
							 .stream()
							 .collect(Collectors.averagingDouble(x -> x));
	}

	@Override
	public <X> InfiniteSequence<X> oneEachInterval(InfiniteSequence<X> iseq, int intervalSize) {
		return () -> {
			int i = 1;
			while(i % intervalSize != 0) {
				iseq.nextElement();
				i++;
			}				
			return iseq.nextElement();
		};
	}

	@Override
	public <X> InfiniteSequence<Boolean> equalsTwoByTwo(InfiniteSequence<X> iseq) {	
		return () -> {
			List<X> listOfTwo = iseq.nextListOfElements(2);
			return listOfTwo.get(0) == listOfTwo.get(1);
		};
	}

	@Override
	public <X, Y extends X> InfiniteSequence<Boolean> equalsOnEachElement(InfiniteSequence<X> isx,
			InfiniteSequence<Y> isy) {
		return () -> isx.nextElement() == isy.nextElement();
	}

	@Override
	public <X> Iterator<X> toIterator(InfiniteSequence<X> iseq) {
		return new Iterator<X>() {
			@Override
			public boolean hasNext() {
				return true;
			}

			@Override
			public X next() {
				return iseq.nextElement();
			}
		};
	}

}
